# uoe-thesis-template

This is the template I used for [my thesis](https://era.ed.ac.uk/handle/1842/36966) in ILCC, School of Informatics, University of Edinburgh, in 2019.
It follows the [basic template provided by the school](https://web.inf.ed.ac.uk/infweb/student-services/igs/phd/exam-process/thesis-latex), with several aesthetical changes.

You can see how that looks in `thesis.pdf`.
